﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PresetManager : MonoBehaviour
{

    // ----------------------------------- Variables
    public enum DeckPreset { preset1 = 1, preset2 = 2, preset3 = 3}
    public DeckPreset _currentPreset;// = DeckPreset.preset1;
    public List<TMP_InputField> deckInputTextList;

    public List<Toggle> preset1Toggles, preset2Toggles, preset3Toggles ;

    string _preset1key = "preset1", _preset2key = "preset2", _preset3key = "preset3", _lastPresetKey = "lastPreset";

    // ----------------------------------- Initialization
    private void Awake()
    {
        // get toggles
        preset1Toggles = GetTogglesWithTag("Preset1Toggle");
        preset2Toggles = GetTogglesWithTag("Preset2Toggle");
        preset3Toggles = GetTogglesWithTag("Preset3Toggle");
    }

    void Start()
    {
        // load last preset; don't use CurrentPreset since it also saves
        DeckPreset _temp = (DeckPreset)PlayerPrefs.GetInt(_lastPresetKey, 1);
        LoadPresetData(_temp);
        _currentPreset = _temp;

        // load correct preset toggle
        switch (CurrentPreset)
        {
            case DeckPreset.preset1:
                TurnOnToggles(true, preset1Toggles);
                break;
            case DeckPreset.preset2:
                TurnOnToggles(true, preset2Toggles);
                break;
            case DeckPreset.preset3:
                TurnOnToggles(true, preset3Toggles);
                break;
            default:
                break;
        }
    }

    // ----------------------------------- Initialization Subfunctions
    List<Toggle> GetTogglesWithTag(string _tag)
    {
        GameObject[] _tempGO = GameObject.FindGameObjectsWithTag(_tag);
        List<Toggle> _tempList = new List<Toggle>();

        for (int _index = 0; _index < _tempGO.Length; _index++)
            _tempList.Add(_tempGO[_index].GetComponentInChildren<Toggle>());

        return _tempList;
    }

    void TurnOnToggles(bool _isOn, List<Toggle> _toggleList)
    {
        foreach(Toggle _toggle in _toggleList)
        {
            _toggle.isOn = _isOn;
        }
    }

    // ----------------------------------- Deconstructor
    void OnApplicationQuit()
    {
        // save last preset
        SavePresetData(_currentPreset);

        // save last open preset
        PlayerPrefs.SetInt(_lastPresetKey, (int)CurrentPreset);
    }

    private void OnApplicationPause(bool pause)
    {
        if (pause)
        {
            // save last preset
            SavePresetData(_currentPreset);

            // save last open preset
            PlayerPrefs.SetInt(_lastPresetKey, (int)CurrentPreset);
        }
    }

    // ----------------------------------- Properties
    public DeckPreset CurrentPreset
    {
        get
        {
            return _currentPreset;
        }
        set
        {
            // save current preset
            SavePresetData(_currentPreset);

            // load new preset
            LoadPresetData(value);

            _currentPreset = value;
        }
    }

    /// <summary>
    /// for getting and setting deck window input field
    /// </summary>
    string DeckInputText
    {
        get
        {
            string _returnString = null;
            foreach (TMP_InputField _text in deckInputTextList)
            {
                if (_text.gameObject != null && _text.gameObject.activeSelf)
                    _returnString = _text.text;
            }

            return _returnString;
        }
        set
        {
            foreach (TMP_InputField _text in deckInputTextList)
            {
                _text.text = value;
                //_text.ForceLabelUpdate();
            }
        }
    }

    // ----------------------------------- Public Functions
    public void ChangePreset(int _presetNum)
    {
        switch (_presetNum)
        {
            case 1:
                CurrentPreset = DeckPreset.preset1;
                break;
            case 2:
                CurrentPreset = DeckPreset.preset2;
                break;
            case 3:
                CurrentPreset = DeckPreset.preset3;
                break;
        }
    }

    // ----------------------------------- Loading and Saving Functions
    void SavePresetData(DeckPreset _presetToSave)
    {
        switch (_presetToSave)
        {
            case DeckPreset.preset1:
                PlayerPrefs.SetString(_preset1key, DeckInputText);
                break;
            case DeckPreset.preset2:
                PlayerPrefs.SetString(_preset2key, DeckInputText);
                break;
            case DeckPreset.preset3:
                PlayerPrefs.SetString(_preset3key, DeckInputText);
                break;
            default:
                break;
        }
    }

    void LoadPresetData(DeckPreset _presetToLoad)
    {
        switch (_presetToLoad)
        {
            case DeckPreset.preset1:
                DeckInputText = PlayerPrefs.GetString(_preset1key, null);
                PlayerPrefs.SetInt(_lastPresetKey, 1);
                break;
            case DeckPreset.preset2:
                DeckInputText = PlayerPrefs.GetString(_preset2key, null);
                PlayerPrefs.SetInt(_lastPresetKey, 2);
                break;
            case DeckPreset.preset3:
                DeckInputText = PlayerPrefs.GetString(_preset3key, null);
                PlayerPrefs.SetInt(_lastPresetKey, 3);
                break;
        }
    }

    public void ResetPresetData()
    {
        // delete keys
        PlayerPrefs.DeleteKey(_preset1key);
        PlayerPrefs.DeleteKey(_preset2key);
        PlayerPrefs.DeleteKey(_preset3key);

        // clear current window
        DeckInputText = "";
    }

    public void AutoSave()
    {
        SavePresetData(CurrentPreset);
        //LogManager.WriteNewLine("AutoSaved");
    }   
}
