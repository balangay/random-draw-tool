﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrientationCanvasChooser : MonoBehaviour
{
    public GameObject landscapeCanvas, portraitCanvas;
    float _checkTime = 0.5f;
    public bool checkingOrientation = true, changeBasedOnOrientation = false;

    void Start()
    {
        if (changeBasedOnOrientation)
            StartCoroutine(OrientationChecker());
    }

    IEnumerator OrientationChecker()
    {
        while (checkingOrientation)
        {
            //Debug.Log(Screen.orientation.ToString());

            if ((Screen.orientation == ScreenOrientation.LandscapeLeft) || (Screen.orientation == ScreenOrientation.LandscapeRight))
            {
                if (portraitCanvas.activeSelf)
                {
                    landscapeCanvas.SetActive(true);
                    portraitCanvas.SetActive(false);
                }
            }
            else if ((Screen.orientation == ScreenOrientation.Portrait) || (Screen.orientation == ScreenOrientation.PortraitUpsideDown))
            {
                if (landscapeCanvas.activeSelf)
                {
                    landscapeCanvas.SetActive(false);
                    portraitCanvas.SetActive(true);
                }
            }

            //if (Screen.orientation == ScreenOrientation.Landscape)
            //{
            //    if (portraitCanvas.activeSelf)
            //    {
            //        landscapeCanvas.SetActive(true);
            //        portraitCanvas.SetActive(false);
            //    }
            //}
            //else if (Screen.orientation == ScreenOrientation.Portrait)
            //{
            //    if (landscapeCanvas.activeSelf)
            //    {
            //        landscapeCanvas.SetActive(false);
            //        portraitCanvas.SetActive(true);
            //    }
            //}
            yield return new WaitForSeconds(_checkTime);
        }
    }
}
