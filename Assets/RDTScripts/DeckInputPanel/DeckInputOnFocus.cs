﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

/// <summary>
/// Sends out event when deck input is selected so keyboard controls can be disabled
/// </summary>
public class DeckInputOnFocus : MonoBehaviour
{
    // ----------------------------------- Fields -----------------------------------
    public delegate void DeckInputEvent();
    public static event DeckInputEvent OnDeckInputSelect, OnDeckInputDeselect;
    public static bool isTyping = false;

    TMP_InputField _deckInput;

    // ----------------------------------- Awake -----------------------------------
    private void Awake()
    {
        _deckInput = GetComponent<TMP_InputField>();
    }

    // ----------------------------------- Event Handling (subscribe in inspector) -----------------------------------
    public void OnSelect()
    {
        //Debug.Log("deck input selected");
        isTyping = true;

        if (OnDeckInputSelect != null)
            OnDeckInputSelect();
    }

    public void OnDeselect()
    {
        //Debug.Log("deck input deselected");
        isTyping = false;
        
        if (OnDeckInputDeselect != null)
            OnDeckInputDeselect();
    }

    // ----------------------------------- Event Handling -----------------------------------
// wont work in webgl, keeping in case i need to copy paste what i did
////#if UNITY_WEBGL
//    // trying to add tab input functionality for webgl build
//    private void Update()
//    {
//        if (isTyping)
//        {
//            if (Input.GetKeyDown(KeyCode.Tab))
//            {
//                LogManager.Write("tab clicked");
                
//            }
//        }
//    }
////#endif

//    public void InsertTab()
//    {
//        // get caret position
//        int _caretPos = _deckInput.caretPosition;

//        // get text
//        string _full = _deckInput.text;

//        // cut string
//        string _firstHalf = _full.Substring(0, _caretPos);
//        string _secondHalf = _full.Substring(_caretPos);

//        _deckInput.text = _firstHalf + "\t" + _secondHalf;

//        _deckInput.caretPosition++;
//    }
}
