﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI.Extensions;

public class KeyboardInput : MonoBehaviour
{
    // ----------------------------------- Variables -----------------------------------
    public HorizontalScrollSnap scrollSnap;
    public float inputInterval = 0.1f;
    float _lastInput;

    public bool disableKeyboardInput = false;

    // ----------------------------------- Initialization -----------------------------------
    private void Start()
    {
        _lastInput = Time.time;
    }

    // ----------------------------------- OnEnable and OnDisable Event Subscribing -----------------------------------
    private void OnEnable()
    {
        DeckInputOnFocus.OnDeckInputSelect += DeckInputOnFocus_OnDeckInputSelect;
        DeckInputOnFocus.OnDeckInputDeselect += DeckInputOnFocus_OnDeckInputDeselect;
    }

    private void OnDisable()
    {
        DeckInputOnFocus.OnDeckInputSelect -= DeckInputOnFocus_OnDeckInputSelect;
        DeckInputOnFocus.OnDeckInputDeselect -= DeckInputOnFocus_OnDeckInputDeselect;
    }

    // ----------------------------------- Event Handling -----------------------------------
    private void DeckInputOnFocus_OnDeckInputSelect()
    {
        disableKeyboardInput = true;
    }

    private void DeckInputOnFocus_OnDeckInputDeselect()
    {
        disableKeyboardInput = false;
    }

    // ----------------------------------- Update -----------------------------------
    void Update()
    {
        // if keyboard control of app stuff is disabled, return
        if (disableKeyboardInput)
            return;

        if (Time.time - _lastInput > inputInterval)
        {
            bool _validButtonEvent = false;

            // arrow button checking
            float _temp = Input.GetAxisRaw("Horizontal");
            if (_temp > float.Epsilon)
            {
                scrollSnap.NextScreen();
                _validButtonEvent = true;
            }
            else if (_temp < -float.Epsilon)
            {
                scrollSnap.PreviousScreen();
                _validButtonEvent = true;
            }

            // spacebar checking
            if (Input.GetKeyDown(KeyCode.Space))
            {
                //Debug.Log("akaka");
                //Debug.Log("SpacebarDraw " + OptionsManager.Instance.SpacebarDraw.ToString());
                //Debug.Log("isrunningsim " + DeckUIManager.IsRunningSimulation.ToString());
                if ( OptionsManager.Instance.SpacebarDraw && DeckUIManager.IsRunningSimulation)
                {
                    //Debug.Log("Space Draw");
                    DeckUIManager.Instance.OnDrawButtonPressed();
                    _validButtonEvent = true;
                }
            }

            if (_validButtonEvent)
                _lastInput = Time.time;
        }
    }
}
