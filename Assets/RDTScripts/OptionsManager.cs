﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI.Extensions;
using UnityEngine.UI;

public class OptionsManager : MonoBehaviour
{
    // ----------------------------------- Variables -----------------------------------
    public HorizontalScrollSnap portraitScrollSnap;
    public Toggle spacebarDrawToggle;

    // ----------------------------------- Initialization -----------------------------------
    private void Awake()
    {
        Instance = this;
        SpacebarDraw = PlayerPrefs.GetInt("SpacebarDraw", 1) == 1;    // 0 for false, 1 for true
        spacebarDrawToggle.isOn = SpacebarDraw;
    }

    // ----------------------------------- Deconstructor
    void OnApplicationQuit()
    {
        // save spacebar draw setting
        if (SpacebarDraw)
            PlayerPrefs.SetInt("SpacebarDraw", 1);
        else
            PlayerPrefs.SetInt("SpacebarDraw", 0);
    }

    void OnApplicationPause(bool paused)
    {
        if (paused)
        {
            // save spacebar draw setting
            if (SpacebarDraw)
                PlayerPrefs.SetInt("SpacebarDraw", 1);
            else
                PlayerPrefs.SetInt("SpacebarDraw", 0);
        }
    }

    // ----------------------------------- Properties -----------------------------------
    public static OptionsManager Instance { get; private set; }
    public bool SpacebarDraw { get; set; }

    // ----------------------------------- Options Functions -----------------------------------
    public void ChangeModulesVisible(int _numberVisible)
    {
        // get rect transform of scroll snap
        RectTransform _rect = portraitScrollSnap.GetComponent<RectTransform>();

        float _anchorMaxX = 1f / (float) _numberVisible;
        float _anchorMaxY = _rect.anchorMax.y;
        Vector2 _newAnchorMax = new Vector2(_anchorMaxX, _anchorMaxY);

        _rect.anchorMax = _newAnchorMax;

        portraitScrollSnap.UpdateLayout();
    }

    //public void RefreshScrollSnap()
    //{
    //    portraitScrollSnap.DistributePages();
    //}
}
