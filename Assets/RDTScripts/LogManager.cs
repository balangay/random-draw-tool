﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class LogManager : MonoBehaviour
{
    // ----------------------------------- Variables
    static LogManager _instance;
    public List<TMP_Text> logTextList;
    public List<ScrollRect> scrollRectList;

    // ----------------------------------- Initialization
    void Start()
    {
        // set singleton
        _instance = this;
    }

    // ----------------------------------- Properties
    public static LogManager Instance
    {
        get
        {
            return _instance;
        }
    }

    public string Text
    {
        get
        {
            string _returnString = null;
            foreach (TMP_Text _text in logTextList)
            {
                if (_text.gameObject.activeSelf)
                {
                    _returnString = _text.text;
                }
            }
            return _returnString;
        }
        set
        {
            foreach (TMP_Text _text in logTextList)
            {
                _text.text = value;
            }
            StartCoroutine(ScrolltoBottom());
        }
    }

    // ----------------------------------- Functions
    public void OnLogClearButtonPressed()
    {
        Text = "";
    }

    public static void WriteNewLine(string _input)
    {
        LogManager.Instance.Text += _input + "\n";
    }

    public static void Write(string _input)
    {
        LogManager.Instance.Text += _input;
    }

    IEnumerator ScrolltoBottom()
    {
        yield return new WaitForEndOfFrame();
        foreach (ScrollRect _scrollRect in scrollRectList)
        {
            _scrollRect.verticalNormalizedPosition = 0f;
        }
    }

}
