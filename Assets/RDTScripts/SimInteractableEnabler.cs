﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SimInteractableEnabler : MonoBehaviour
{
    // ----------------------------------- Fields -----------------------------------
    Selectable _simInteractable;
    public bool isInteractableDuringSim = true;

    // ----------------------------------- Awake -----------------------------------
    private void Awake()
    {
        _simInteractable = GetComponent<Selectable>();
        //_simInteractable.interactable = !isInteractableDuringSim;
    }

    // ----------------------------------- Event Subscribing -----------------------------------
    private void OnEnable()
    {
        DeckUIManager.OnSimStart += DeckUIManager_OnSimStart;
        DeckUIManager.OnSimEnd += DeckUIManager_OnSimEnd;

        if (DeckUIManager.IsRunningSimulation)
            _simInteractable.interactable = isInteractableDuringSim;
        else
            _simInteractable.interactable = !isInteractableDuringSim;
    }

    private void OnDisable()
    {
        DeckUIManager.OnSimStart -= DeckUIManager_OnSimStart;
        DeckUIManager.OnSimEnd -= DeckUIManager_OnSimEnd;
    }

    // ----------------------------------- Event Handling -----------------------------------
    private void DeckUIManager_OnSimStart()
    {
        _simInteractable.interactable = isInteractableDuringSim;
    }

    private void DeckUIManager_OnSimEnd()
    {
        _simInteractable.interactable = !isInteractableDuringSim;
    }
}
