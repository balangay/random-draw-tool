﻿using System;
using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class Card
{
	// ----------------------------------- Variables
	public string name;
	string _cardText;
	public CardType cardType;   // reference to the cardtype list in the deck. this makes it easier to keep track of quantities in a deck since you can modify the cardtype directly

	// ----------------------------------- Constructor
	public Card()
	{
		name = "-";
		_cardText = "-";
	}

	public Card(string _nameIn, string _cardTextIn)
	{
		name = _nameIn;
		_cardText = _cardTextIn;
		cardType = null;
	}

	public Card(string _nameIn, string _cardTextIn, CardType _cardTypeIn)
	{
		name = _nameIn;
		_cardText = _cardTextIn;
		cardType = _cardTypeIn;
	}

	// ----------------------------------- Properties
	public string CardText
	{
           get
           {
			return _cardText;
           }
	}

	// ----------------------------------- Static Functions
	public static bool IsSameCardType(Card _card1, Card _card2)
    {
		if (_card1 == null || _card2 == null)
			return false;

		if (_card1.name == _card2.name && _card1.CardText == _card2.CardText)
		{
			return true;
		}
		else
			return false;
    }

	// ----------------------------------- Functions
    public Card CreateCopy()
    {
		return new Card(name, _cardText, cardType);
    }

    public void ConnectToCardTypeInDeck(Deck _deckin)
    {
		// get card type list
		List<CardType> _tempList = _deckin.cardTypesList;

        // find the correct cardtype in the list
        foreach (CardType _type in _tempList)
        {
            if (_type.IsSameCardType(this))
            {
				cardType = _type;
            }
        }
    }
}

[System.Serializable]
public class CardType
{
	// ----------------------------------- Variables
	public Card cardArchetype;
	public int deckCount, remainingDeckCount;
	Deck _deck;     // the deck that has this cardtype

	// ----------------------------------- Constructor
	public CardType(Card _cardIn, Deck _deckIn, int _deckCountInt)
	{
		// get the card details
		cardArchetype = _cardIn;

        // get the deck that contains the cards
		_deck = _deckIn;

        // initialize input of cards
		deckCount = _deckCountInt;
		ResetCount();
	}

	// ----------------------------------- Functions
	/// <summary>
	/// computes the probability of drawing this kind of card
	/// </summary>
	/// <returns></returns>
	public float DrawProbability()
    {
		if (_deck.RemainingCards == 0)
			return 0f;

        return ((float)remainingDeckCount) / _deck.RemainingCards;
    }

    public void ResetCount()
    {
		remainingDeckCount = deckCount;
    }

    public bool IsSameCardType(Card _inputCard)
    {
		return Card.IsSameCardType(cardArchetype, _inputCard);
    }

	public CardType CreateCopy()
	{
		return new CardType(cardArchetype, _deck, deckCount);
	}

    public void CopyCardTypeQtyDataTo(CardType _targetCardType)
    {
		_targetCardType.deckCount = deckCount;
		_targetCardType.remainingDeckCount = remainingDeckCount;
    }
}

