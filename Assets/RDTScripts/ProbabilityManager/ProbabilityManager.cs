﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ProbabilityManager : MonoBehaviour
{
    // ----------------------------------- Variables
    static ProbabilityManager _instance;

    public List<ProbabilityPanelMgr> probPanels;
    public Transform probContentGO;
    public GameObject probPanelPrefab;
    public Deck deck;
    Color _zeroColor = new Color(0.2941177f, 0.2941177f, 0.2941177f);

    public TMP_Text /*lastCardText,*/ sumAText, sumBText, sumCText;

    // ----------------------------------- Initialization and Destruction
    public ProbabilityManager() {
        _instance = this;
    }

    void OnEnable()
    {
        // initialize list
        probPanels = new List<ProbabilityPanelMgr>();

        // add DeckUIManager events
        DeckUIManager.OnSimStart += OnSimStartHandler;
        DeckUIManager.OnSimEnd += OnSimEndHandler;

        // add deck events
        Deck.Instance.OnCardDrawn += OnCardDrawnHandler;
        Deck.Instance.OnDeckCompoChanged += OnCardDrawnHandler;
        Deck.Instance.OnResetRemainingCardCounts += OnResetRemainingCardCountsHandler;

        // Update prob sum groups
        UpdateSumGroupProbs();
    }

    private void OnDisable()
    {
        // remove DeckUIManager
        DeckUIManager.OnSimStart -= OnSimStartHandler;
        DeckUIManager.OnSimEnd -= OnSimEndHandler;

        // remove deck events
        Deck.Instance.OnCardDrawn -= OnCardDrawnHandler;
        Deck.Instance.OnDeckCompoChanged -= OnCardDrawnHandler;
        Deck.Instance.OnResetRemainingCardCounts -= OnResetRemainingCardCountsHandler;
    }

    // ----------------------------------- Event Handling
    /// <summary>
    /// Initializes the probability manager when the simulation start event is called by DeckUIManager
    /// </summary>
    private void OnSimStartHandler()
    {
        // get number of card types then populate prob panel list
        for (int _ctr = 0; _ctr < deck.CardTypes; _ctr++)
        {
            // instantaite panel prefab
            ProbabilityPanelMgr _temp = Instantiate(probPanelPrefab, probContentGO, false).GetComponent<ProbabilityPanelMgr>();

            // add panel prefab to prob panels list
            probPanels.Add(_temp);

            // initialize panel
            _temp.InitializeProbPanel(deck.cardTypesList[_ctr]);

            //// init last card
            //lastCardText.text = "(none drawn yet)";
        }
    }

    /// <summary>
    /// Cleanup when a sim is ended. Destroy prob panels and new list
    /// </summary>
    private void OnSimEndHandler()
    {
        // destroy probabilty panels
        foreach (ProbabilityPanelMgr _panel in probPanels)
        {
            // destroy each active panel (i keep a prefab inactive for easy editing's take)
            if (_panel.gameObject.activeSelf)
            {
                Destroy(_panel.gameObject);
            }
        }

        // initialize list
        probPanels = new List<ProbabilityPanelMgr>();

        // init sum group text
        sumAText.text = "A: 0%";
        sumBText.text = "B: 0%";
        sumCText.text = "C: 0%";
    }

    /// <summary>
    /// Updates the sum group computations.
    /// </summary>
    /// <param name="_cardArg"></param>
    private void OnCardDrawnHandler(Card _cardArg)
    {
        // only update sum group probabilities since the indiv panels will handler their own oncarddrawn events
        UpdateSumGroupProbs();
    }

    private void OnResetRemainingCardCountsHandler(Card _cardArg)
    {
        UpdateAll();
    }

    // ----------------------------------- Properties
    public static ProbabilityManager Instance
    {
        get
        {
            return _instance;
        }
    }

    // ----------------------------------- UI Functions
    /// <summary>
    /// For when necessary, updates all output UI to state of the deck
    /// </summary>
    public void UpdateAll()
    {
        // update sum group probs
        UpdateSumGroupProbs();

        // update each panel
        foreach (ProbabilityPanelMgr _panel in probPanels)
        {
            _panel.UpdateOutput();
        }
    }

    // ----------------------------------- Sum Group Functions

    public void UpdateSumGroupProbs()
    {
        // get probability sums
        float _sumA = ComputeSumGroupProb(ProbabilitySumGroup.A);
        float _sumB = ComputeSumGroupProb(ProbabilitySumGroup.B);
        float _sumC = ComputeSumGroupProb(ProbabilitySumGroup.C);

        // update probability sum groups
        sumAText.text = "A: " + _sumA.ToString("P0");
        sumBText.text = "B: " + _sumB.ToString("P0");
        sumCText.text = "C: " + _sumC.ToString("P0");

        // change text color
        if (_sumA == 0f)
            sumAText.color = _zeroColor;
        else
            sumAText.color = Color.black;

        if (_sumB == 0f)
            sumBText.color = _zeroColor;
        else
            sumBText.color = Color.black;

        if (_sumC == 0f)
            sumCText.color = _zeroColor;
        else
            sumCText.color = Color.black;
    }

    float ComputeSumGroupProb (ProbabilitySumGroup _group)
    {
        float _probability = 0f;

        // run through panels to check if it's part of the sum group then add value to probability running sum
        foreach (ProbabilityPanelMgr _probPanel in probPanels)
        {
            // check if part of group
            if (_probPanel.currentSumGroup == _group)
            {
                _probability += _probPanel.associatedCardType.DrawProbability();
            }
        }

        return _probability;
    }
}
