﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

// enum
public enum ProbabilitySumGroup { None = 0, A = 1, B = 2, C = 3 }

public class ProbabilityPanelMgr : MonoBehaviour
{
    // ----------------------------------- Variables
    public TMP_Text qtyText, fullQtyText, probText, cardName;
    public CardType associatedCardType;
    public ProbabilitySumGroup currentSumGroup = ProbabilitySumGroup.None;
    Color _zeroColor = new Color(0.2941177f, 0.2941177f, 0.2941177f, 1f),
        _sumAColor = new Color(1f, 0.5019608f, 0.5019608f, 0.7529412f),
        _sumBColor = new Color(0.4980392f, 1f, 0.5398692f, 0.7490196f),
        _sumCColor = new Color(0.5019608f, 0.6264706f, 1f, 0.7490196f);

    // ----------------------------------- Initialization
    void OnEnable()
    {
        // add on card drawn event
        Deck.Instance.OnCardDrawn += OnCardDrawnHandler;
        Deck.Instance.OnDeckCompoChanged += OnCardDrawnHandler;
    }

    public void OnDisable()
    {
        // remove on card drawn event
        Deck.Instance.OnCardDrawn -= OnCardDrawnHandler;
        Deck.Instance.OnDeckCompoChanged -= OnCardDrawnHandler;

    }

    public void InitializeProbPanel(CardType _cardTypeIn)
    {
        // save card type
        associatedCardType = _cardTypeIn;

        // get full qty
        fullQtyText.text = associatedCardType.deckCount.ToString();

        // set card name
        cardName.text = associatedCardType.cardArchetype.name;

        // set start qty and probability
        UpdateOutput();

        // reset color of prob text
        probText.color = Color.black;
        qtyText.color = Color.black;
    }

    // ----------------------------------- Button Functions
    public void ChangeSumGroup(int _grp)
    {
        currentSumGroup = (ProbabilitySumGroup) _grp;

        // update sum group sums
        ProbabilityManager.Instance.UpdateSumGroupProbs();
    }

    // ----------------------------------- UI Subfunctions
    /// <summary>
    /// Updates the probability and quantity values on the panels.
    /// </summary>
    public void UpdateOutput()
    {
        // update prob text
        probText.text = associatedCardType.DrawProbability().ToString("P0");

        // update qty text
        qtyText.text = associatedCardType.remainingDeckCount.ToString();

        // get full qty
        fullQtyText.text = associatedCardType.deckCount.ToString();

        // change color if 0
        if (associatedCardType.remainingDeckCount == 0)
        {
            probText.color = _zeroColor;
            qtyText.color = _zeroColor;
        }
    }

    // ----------------------------------- Event Handlers
    /// <summary>
    /// updates probability stats when a card is drawn
    /// </summary>
    /// <param name="_cardArg"></param>
    private void OnCardDrawnHandler(Card _cardArg)
    {
        // update panel. even if it isn't the card drawn, the probability will change
        UpdateOutput();
    }
}
