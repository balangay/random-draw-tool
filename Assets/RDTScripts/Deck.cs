﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

/// <summary>
/// Events should only be for sending out events to output UI modules and
/// not for changing the internal state of the Deck. This is to make sure that
/// when an event is sent to output UI modules, the internal computations and
/// state is already the most updated.
/// </summary>
public class Deck : MonoBehaviour
{
	// ----------------------------------- Variables
	public List<Card> cards, discard;
	Card _lastDrawn;
	public List<CardType> cardTypesList;
	public static string[] lineSeparators = { "\t", "   ", ".  " };    // characters or strings used to separate the qty, card name, and card text in a line
	static Deck _instance;   // singleton

	// events
    /// <summary>
    /// OnCardDrawn - when a card drawn from the top
    /// OnResetRemainingCardCount - called when the deck is reset
    /// OnDeckCompoChanged - called for misc events that change the deck compo that doesnt fit with more specific events
    /// </summary>
    /// <param name="_cardArg"></param>
	public delegate void DeckOutputUpdateEvent(Card _cardArg);
	public event DeckOutputUpdateEvent OnCardDrawn, OnResetRemainingCardCounts, OnDeckCompoChanged;

	// storage to store raw deck data after parsing
	public List<CardType> _originalDecklistCardTypes;

	// ----------------------------------- Initialization
	public Deck()
	{
		_instance = this;
		ResetDeck();
	}

    public void ResetDeck()
    {
		cards = new List<Card>();
		discard = new List<Card>();
		cardTypesList = new List<CardType>();
		_lastDrawn = null;
	}

	// ----------------------------------- Properties
	public static Deck Instance
	{
		get
		{
			return _instance;
		}
	}

	public int RemainingCards
	{
		get
		{
			return cards.Count;
		}
	}

	public int DiscardCount
	{
		get
		{
			return discard.Count;
		}
	}

	public int CardTypes
	{
		get
		{
			return cardTypesList.Count;
		}
	}

	public Card LastDrawnCard
	{
		get
		{
			return _lastDrawn;
		}
	}

	// ----------------------------------- Building Block Deck Functions
	// basic commands that become the building blocks of more complex and specific commands

    /// <summary>
    /// Resets the deck to what's on the raw decklist string data. Not shuffled.
    /// </summary>
	public void ResetDeckToOriginalParsedData()
	{
		// reset card lists (don't reset card type lists since they are referenced by other codes)
		cards = new List<Card>();
		discard = new List<Card>();
		_lastDrawn = null;

		// copy cardtype qty from original to curret (assuming same card types have same indices)
		for (int _index = 0; _index < cardTypesList.Count; _index++)
		{
			_originalDecklistCardTypes[_index].CopyCardTypeQtyDataTo(cardTypesList[_index]);
		}

		// repopulate deck based on card types
		for (int _index = 0; _index < cardTypesList.Count; _index++)
        {
			// get card archetype
			Card _typeArchetype = cardTypesList[_index].cardArchetype;

            for (int _cardCount = 0; _cardCount < cardTypesList[_index].deckCount; _cardCount++)
            {
				cards.Add(_typeArchetype.CreateCopy());
            }
        }

		// send Restart Deck event
		if (OnResetRemainingCardCounts != null)
			OnResetRemainingCardCounts(null);
	}

	/// <summary>
	/// Adds a card to the deck. It adds to the top of the deck because it has to add it somewhere.
	/// Doesn't add to the card types. Assumes the method that calls this will take care of that.
	/// </summary>
	/// <param name="_card"></param>
	public void AddCard(Card _card)
	{
		cards.Add(_card);
		_card.ConnectToCardTypeInDeck(this);
	}

    /// <summary>
    /// Returns the cards in the discard list to the deck.
    /// Then, updates card types and sends out restart event
    /// </summary>
	public void ReturnDiscardToDeck()
	{
		// return discarded to deck
		while (discard.Count > 0)
		{
			cards.Add(discard[0]);
			discard.RemoveAt(0);
		}

		// reset card type counts
		foreach (CardType _cardType in cardTypesList)
		{
			_cardType.ResetCount();
		}

		// send Restart Deck event
		if (OnResetRemainingCardCounts != null)
			OnResetRemainingCardCounts(null);
	}

	/// <summary>
	/// Shuffles the cards remaining in the deck
	/// </summary>
	public void ShuffleCards()
	{
		/* Fisher Yates
		--To shuffle an array a of n elements(indices 0..n - 1):
		for i from n−1 downto 1 do
				j ← random integer such that 0 ≤ j ≤ i
				exchange a[j] and a[i]
		*/
		System.Random _random = new System.Random();
		int _randomInt;
		Card _temp;
		for (int ctr = cards.Count - 1; ctr > 0; ctr--)
		{
			_randomInt = _random.Next(0, ctr + 1);

			// exchange cards[_random] and cards[ctr]
			_temp = cards[_randomInt];
			cards[_randomInt] = cards[ctr];
			cards[ctr] = _temp;
		}
	}

	/// <summary>
	/// Adds a card to the deck. If _cardsFromTheTop is 0, card is added to the top.
	/// Be careful when using this as the added card comes from nowhere. Potentially,
    /// this function can destroy the deck list since after a reshuffle, the added card
    /// is added to the new deck.
	/// </summary>
	/// <param name="_cardsFromTheTop"></param>
	/// <returns></returns>
	public void InsertCardAt(Card _card, int _cardsFromTheTop, bool _updateCardTypeCounts)
	{
        // get index
		int _index = RemainingCards - _cardsFromTheTop - 1;

        // insert card at index
		InsertCardAtIndex(_card, _index, _updateCardTypeCounts);
	}

	/// <summary>
	/// Adds a card to the deck at the index.
	/// Be careful when using this as the added card comes from nowhere. Potentially,
	/// this function can destroy the deck list since after a reshuffle, the added card
	/// is added to the new deck.
	/// </summary>
	/// <param name="_cardsFromTheTop"></param>
	/// <returns></returns>
	public void InsertCardAtIndex(Card _card, int _index, bool _updateCardTypeCounts)
    {
		// add card
		cards.Insert(_index, _card);

		// connect card to deck's cardtype
		_card.ConnectToCardTypeInDeck(this);

		// if input is true, update card type, increase remaining and deck count
		if (_updateCardTypeCounts)
		{
			_card.cardType.deckCount++;
			_card.cardType.remainingDeckCount++;
		}

		// call generic event that the deck has changed
		if (OnDeckCompoChanged != null)
			OnDeckCompoChanged(null);
	}

    /// <summary>
    /// Returns the card peeked at. If _cardsFromTheTop is 0, look at top card.
    /// Returns null if deck is empty. Be careful in manipulating the return as
    /// it is a reference to the actual card peeked at.
    /// </summary>
    /// <param name="_cardsFromTheTop"></param>
    /// <returns></returns>
	public Card PeekAtCardAt(int _cardsFromTheTop)
	{
		int _index = RemainingCards - _cardsFromTheTop - 1;

		// if no more cards remaining, return null
		if (_index == -1)
		{
			return null;
		}

		return cards[_index];
	}

	/// <summary>
	/// Removes a card from the deck. If _cardsFromTheTop is 0, top card is removed.
	/// Be careful when using this as the added card comes from nowhere. Potentially,
	/// this function can destroy the deck list since after a reshuffle, the removed card
	/// won't be added to the new deck.
	/// </summary>
	/// <param name="_cardsFromTheTop"></param>
	/// <param name="_updateCardTypeCounts"></param>
	public void RemoveCardAt(int _cardsFromTheTop, bool _updateCardTypeCounts)
	{
        // compute index
		int _index = RemainingCards - _cardsFromTheTop - 1;

		// remove card at index
		RemoveCardAtIndex(_index, _updateCardTypeCounts);
	}

	/// <summary>
	/// Removes a card from the deck at the index.
	/// Be careful when using this as the added card comes from nowhere. Potentially,
	/// this function can destroy the deck list since after a reshuffle, the removed card
	/// won't be added to the new deck.
	/// </summary>
	/// <param name="_cardsFromTheTop"></param>
	/// <param name="_updateCardTypeCounts"></param>
	public void RemoveCardAtIndex(int _index, bool _updateCardTypeCounts)
    {
		// get card to be removed
		Card _temp = cards[_index];

		// remove card
		cards.RemoveAt(_index);

		// update card types depending on setting
		if (_updateCardTypeCounts)
		{
			_temp.cardType.remainingDeckCount--;
			_temp.cardType.deckCount--;
		}

		// call generic event that the deck has changed
		if (OnDeckCompoChanged != null)
			OnDeckCompoChanged(null);
	}

    /// <summary>
    /// Returns an index of a card in the deck that matches the target card.
    /// </summary>
    /// <param name="_targetCard"></param>
    /// <returns></returns>
    public int FindCardType(Card _targetCard)
    {
        // find a card that matches the card
		return cards.FindIndex(_card => Card.IsSameCardType(_card, _targetCard));
    }

	// ----------------------------------- Specific Deck Command Functions
	// common deck functions

	/// <summary>
	/// Return the discard pile to the deck then shuffle it
	/// </summary>
	public void ReturnDiscardToDeckThenShuffle()
	{
		// return discarded to deck
		ReturnDiscardToDeck();

		// shuffle cards
		ShuffleCards();
	}

    /// <summary>
    /// Reset the card list and card type lists to the original parsed data specs
    /// then shuffle it.
    /// </summary>
    public void ResetDeckThenShuffle()
    {
		// reset deck to original deck list
		ResetDeckToOriginalParsedData();

		// shuffle cards
		ShuffleCards();
    }

	public Card DrawCard()
	{
		// get index of last card
		int _index = cards.Count - 1;

		// if no more cards remaining, return null
		if (_index == -1)
		{
			return null;
		}

		// get the card with the highest index
		_lastDrawn = cards[_index];

		// remove the card from the deck
		cards.RemoveAt(_index);

		// add card to discard pile
		discard.Add(_lastDrawn);

		// update card types count
		_lastDrawn.cardType.remainingDeckCount--;

		// event
		if (OnCardDrawn != null)
			OnCardDrawn(_lastDrawn);

		// return
		return _lastDrawn;
	}

	// ----------------------------------- Parse Functions
	/// <summary>
	/// Parses the deck window input
	/// </summary>
	/// <returns> Returns 0 if success. Returns the line where there is a mistake if there is a mistake in the inpuit.</returns>
	/// <param name="_rawText">Raw text.</param>
	public int ParseDeckCSV(string _rawText)
	{
		// split text block into lines
		string[] _textLines = _rawText.Split(new[] { '\r', '\n' });

		// empty lines counter
		int _whiteSpaceLines = 0;

		// parse each line and create x cards
		for (int i = 0, _textLinesLength = _textLines.Length; i < _textLinesLength; i++)
		{
			string _cardLine = _textLines[i];

            if (String.IsNullOrWhiteSpace(_cardLine))
            {
				LogManager.WriteNewLine("Warning: Line " + (i + 1).ToString() + " is empty. Skipping...");
				_whiteSpaceLines++;
				continue;
            }

			// parse line
			string[] _cardInfo = _cardLine.Split(lineSeparators, StringSplitOptions.RemoveEmptyEntries);

			//Debug.Log(_cardInfo.Length);
			//Debug.Log((_cardInfo.Length != 3).ToString());
			//Debug.Log((i + 1).ToString());

			// check if there is an error in the line
			if (_cardInfo.Length != 3)
			{
				LogManager.WriteNewLine("Error found in line " + (i + 1).ToString());
				LogManager.WriteNewLine("Parsing and Simulation Stopped.\n" +
					"Use Tab, Space-Space-Space (\"   \"), and/or Period-Space-Space (\".  \") to separate the Qty, Card Name, and Card Text.\n");
				return i + 1;
			}

			// get quantity
			int _qty = 0;
            if (!Int32.TryParse(_cardInfo[0], out _qty))
            {
				LogManager.WriteNewLine("Error: In line " + (i + 1).ToString() + ": First value for quantity is not an integer. Parsing and Simulation Stopped.");
				return i + 1;
			}

			// create card archetype for the card type
			Card _cardArchetype = new Card(_cardInfo[1], _cardInfo[2]);

			// create card type then connect card archetype to card tyoe
			CardType _newCardType = new CardType(_cardArchetype, this, _qty);
			_cardArchetype.cardType = _newCardType;

            // add card type to list
			cardTypesList.Add(_newCardType);

			// add cards to deck equal to the quantity
			for (int _cardCount = 0; _cardCount < _newCardType.deckCount; _cardCount++)
			{
				cards.Add(_newCardType.cardArchetype.CreateCopy());
			}

			//for (int _ctr = 0; _ctr < _qty; _ctr++)
			//{
			//	AddCard(new Card(_cardInfo[1], _cardInfo[2], _newCardType));
			//}
		}

		// save raw data if successful. cant use a simple copy because the references will also be saved
		_originalDecklistCardTypes = new List<CardType>();
		for (int _index = 0; _index < cardTypesList.Count; _index++)
		{
			_originalDecklistCardTypes.Add(cardTypesList[_index].CreateCopy());
		}

		// return 0 is success
		return 0;
	}
}

