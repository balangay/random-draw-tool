﻿using System.Collections;
using UnityEngine;
using UnityEngine.Advertisements;

public class InitializeAdsScript : MonoBehaviour
{
    public enum platform {iOS, Android}

    string gameIdiOS = "3532920";
    string gameIdAndroid = "3532921";
    public string gameId;
    public string placementId = "Banner";
    public bool testMode = true;
    public platform currentPlatform = platform.iOS;

    void Start()
    {
        if (currentPlatform == platform.iOS)
            gameId = gameIdiOS;
        else if (currentPlatform == platform.Android)
            gameId = gameIdAndroid;

        Advertisement.Initialize(gameIdiOS, testMode);
        StartCoroutine(ShowBannerWhenReady());
    }

    IEnumerator ShowBannerWhenReady()
    {
        while (!Advertisement.IsReady(placementId))
        {
            yield return new WaitForSeconds(0.5f);
        }
        Advertisement.Banner.SetPosition(BannerPosition.TOP_CENTER);
        Advertisement.Banner.Show(placementId);
    }
}