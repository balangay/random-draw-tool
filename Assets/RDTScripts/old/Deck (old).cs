﻿//using System;
//using System.Collections.Generic;
//using RandomDrawTool;

//namespace RandomDrawTool
//{
//	public class Deck
//	{
//		// ----------------------------------- Variables
//		public List<Card> cards;
//		public List<Card> discard;
//        public static string[] lineSeparators = { "\t", "   " };    // characters or strings used to separate the qty, card name, and card text in a line

//        // ----------------------------------- Constructor
//        public Deck()
//		{
//			cards = new List<Card>();
//			discard = new List<Card>();
//		}

//		// ----------------------------------- Properties
//		public int RemainingCards
//		{
//			get
//			{
//				return cards.Count;
//			}
//		}

//		public int DiscardCount
//		{
//			get
//			{
//				return discard.Count;
//			}
//		}

//		// ----------------------------------- Deck Functions
//		public void AddCard(Card _card)
//		{
//			cards.Add(_card);
//		}

//		public void ShuffleCards()
//		{
//			// return discarded to deck
//			while (discard.Count > 0)
//			{
//				cards.Add(discard[0]);
//				discard.RemoveAt(0);
//			}

//			/* Fisher Yates
//			--To shuffle an array a of n elements(indices 0..n - 1):
//			for i from n−1 downto 1 do
//					j ← random integer such that 0 ≤ j ≤ i
//					exchange a[j] and a[i]
//			*/
//			Random _random = new Random();
//			int _randomInt;
//			Card _temp;
//			for (int ctr = cards.Count - 1; ctr > 0; ctr--)
//			{
//				_randomInt = _random.Next(0, ctr+1);

//				// exchange cards[_random] and cards[ctr]
//				_temp = cards[_randomInt];
//				cards[_randomInt] = cards[ctr];
//				cards[ctr] = _temp;
//			}
//		}

//		public Card DrawCard()
//		{
//			// get index of last card
//			int _index = cards.Count - 1;

//			// if no more cards remaining, return null
//			if (_index == -1)
//			{
//				return null;
//			}

//			// get the card with the highest index
//			Card _drawn = cards[_index];

//			// remove the card from the deck
//			cards.RemoveAt(_index);

//			// add card to discard pile
//			discard.Add(_drawn);

//			// return
//			return _drawn;
//		}

//		// ----------------------------------- Parse Functions
//		/// <summary>
//        /// Parses the deck window input
//        /// </summary>
//        /// <returns> A deck if okay. Null if there is a mistake in the input.</returns>
//        /// <param name="_rawText">Raw text.</param>
//        public static Deck ParseDeckCSV(string _rawText)
//		{
//			// create new deck
//			Deck _deck = new Deck();

//			// split text block into lines
//			string[] _textLines = _rawText.Split(new[] { '\r', '\n' });

//            // parse each line and create x cards
//            for (int i = 0, _textLinesLength = _textLines.Length; i < _textLinesLength; i++)
//            {
//                string _cardLine = _textLines[i];

//                // parse line
//                string[] _cardInfo = _cardLine.Split(lineSeparators, StringSplitOptions.RemoveEmptyEntries);

//                // check if there is an error in the line
//                if (_cardInfo.Length != 3){
//                    LogManager.WriteNewLine("Error found in line " + (i + 1).ToString());
//                    return null;
//                }

//                // get quantity
//                int _qty = Int32.Parse(_cardInfo[0]);

//                // create card
//                Card _newCard = new Card(_cardInfo[1], _cardInfo[2]);

//                // add cards to deck equal to the quantity
//                for (int _ctr = 0; _ctr < _qty; _ctr++)
//                {
//                    _deck.AddCard(_newCard);
//                }
//            }

//            // return deck
//            return _deck;
//		}
//	}
//}

