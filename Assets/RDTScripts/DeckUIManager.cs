﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using TMPro;

/// <summary>
/// Handles the big UI buttons and that interact with the Deck.
/// Deck holds the actual computations and events to update the other output UI
/// components just to make sure output is reflecting the latest state of the
/// deck.
///
/// Only event DeckUIManager has is OnSimStart and OnSimEnd since these are
/// primarily UI events and not deck state events.
/// </summary>
public class DeckUIManager : MonoBehaviour
{
    // ----------------------------------- Variables
    public Deck deck;
    int _draw;

    // UI elements
    public TMP_Text errorDisplay;
    public List<TMP_Text> remainingTextList, discardTextList, drawnCardNameList, drawnCardAbilityList;
    public List<TMP_InputField> deckInputTextList;  
    //public List<Selectable> simSelectables;
    public List<Button> startSimButtons, stopSimButtons;

    // events
    public delegate void DeckUIManagerEvent();
    public static event DeckUIManagerEvent OnSimStart, OnSimEnd;

    // ----------------------------------- Initialization -----------------------------------
    private void Awake()
    {
        // init ui text pro lists
        remainingTextList = InitTextProListWithTag("RemainingText");
        discardTextList = InitTextProListWithTag("DiscardText");
        drawnCardNameList = InitTextProListWithTag("DrawnCardNameText");
        drawnCardAbilityList = InitTextProListWithTag("DrawnCardAbilityText");

        // init ui text selectable lists
        //simSelectables = InitSelectableListWithTag("SimulationSelectable");
        //EnableSimSelectables(false);

        // init ui sim button lists
        startSimButtons = InitButtonListWithTag("StartSimButton");
        stopSimButtons = InitButtonListWithTag("StopSimButton");
        ToggleSimulationButtons(false);

        // singleton
        Instance = this;
        IsRunningSimulation = false;
    }

    private void OnEnable()
    {
        // initialize word wrapping on input field to override default
        foreach (TMP_InputField _textInput in deckInputTextList)
        {
            _textInput.textComponent.enableWordWrapping = false;
        }

        // simulation isn't running at start-up
        IsRunningSimulation = false;
    }

    // ----------------------------------- List init by tag -----------------------------------
    private List<TMP_Text> InitTextProListWithTag(string _tag)
    {
        GameObject[] _tempGO = GameObject.FindGameObjectsWithTag(_tag);
        List<TMP_Text> _tempList = new List<TMP_Text>();

        for (int _index = 0; _index < _tempGO.Length; _index++)
            _tempList.Add(_tempGO[_index].GetComponentInChildren<TMP_Text>());

        return _tempList;
    }

    private List<Selectable> InitSelectableListWithTag(string _tag)
    {
        GameObject[] _tempGO = GameObject.FindGameObjectsWithTag(_tag);
        List<Selectable> _tempList = new List<Selectable>();

        for (int _index = 0; _index < _tempGO.Length; _index++)
            _tempList.Add(_tempGO[_index].GetComponentInChildren<Selectable>());

        return _tempList;
    }

    private List<Button> InitButtonListWithTag(string _tag)
    {
        GameObject[] _tempGO = GameObject.FindGameObjectsWithTag(_tag);
        List<Button> _tempList = new List<Button>();

        for (int _index = 0; _index < _tempGO.Length; _index++)
            _tempList.Add(_tempGO[_index].GetComponentInChildren<Button>());

        return _tempList;
    }

    // ----------------------------------- UI Properties
    public static DeckUIManager Instance { get; private set; }
    public static bool IsRunningSimulation { get; private set; }

    /// <summary>
    /// to change remaining text display
    /// </summary>
    string RemainingText
    {
        set
        {
            foreach (TMP_Text _text in remainingTextList)
            {
                _text.text = value;
            }
        }
    }

    /// <summary>
    /// to change discard text display
    /// </summary>
    string DiscardText
    {
        set
        {
            foreach (TMP_Text _text in discardTextList)
            {
                _text.text = value;
            }
        }
    }

    /// <summary>
    /// to change drawn card name display
    /// </summary>
    string DrawnCardName
    {
        set
        {
            foreach (TMP_Text _text in drawnCardNameList)
            {
                _text.text = value;
            }
        }
    }

    /// <summary>
    /// to change drawn card text display
    /// </summary>
    string DrawnCardText
    {
        set
        {
            foreach (TMP_Text _text in drawnCardAbilityList)
            {
                _text.text = value;
            }
        }
    }

    /// <summary>
    /// for getting and setting deck window input field
    /// </summary>
    string DeckInputText
    {
        get
        {
            string _returnString = null;
            foreach (TMP_InputField _text in deckInputTextList)
            {
                if (_text.gameObject.activeSelf)
                    _returnString = _text.text;
            }

            return _returnString;
        }
        set
        {
            foreach (TMP_InputField _text in deckInputTextList)
            {
                _text.text = value;
            }
        }
    }

    // ----------------------------------- Button Event Handlers
    public void OnSimulationStateButtonPressed(bool _startSim)
    {
        if (_startSim)
        {
            // check if deck is valid
            if (StartSimulation())
            {
                ToggleSimulationButtons(_startSim);
                IsRunningSimulation = true;
            }
            else
            {
                errorDisplay.text = "Error. Check Log.";
            }
        }
        else
        {
            StopSimulation();
            ToggleSimulationButtons(_startSim);
            IsRunningSimulation = false;
        }
    }

    void ToggleSimulationButtons(bool _startSim)
    {
        foreach (Button _button in startSimButtons)
            _button.gameObject.SetActive(!_startSim);

        foreach (Button _button in stopSimButtons)
            _button.gameObject.SetActive(_startSim);
    }

    /// <summary>
    /// Handling the Draw Button. Trying to keep all UI-related algos here while card, deck, and data algos in deck.DrawCard()
    /// </summary>
    public void OnDrawButtonPressed()
    {
        if (IsRunningSimulation)
        {
            // get card from deck
            Card _card = deck.DrawCard();

            // if deck is empty, display
            if (_card == null)
            {
                LogManager.WriteNewLine("Deck has run out of cards.");
                return;
            }

            // display card
            LogManager.WriteNewLine("Draw " + _draw + ":\t" + _card.name);

            // display card ability
            DrawnCardName = _card.name;
            DrawnCardText = _card.CardText;

            // refresh remaining and discard
            RemainingText = deck.RemainingCards.ToString();
            DiscardText = deck.DiscardCount.ToString();

            // increment draw
            _draw++;
        }
    }

    public void OnRestartButtonPressed()
    {
        if (IsRunningSimulation)
        {
            RestartSimulation();
        }
    }

    // ----------------------------------- Simulation Functions
    bool StartSimulation()
    {
        // write start simulation text
        LogManager.WriteNewLine("Starting Simulation...");

        // parse deck details
        LogManager.WriteNewLine("Parsing Deck Data...");

        // check if deck input is empty. if empty, get deck text from placeholder
        int _errorLine = 0;
        if (!string.IsNullOrEmpty(DeckInputText))
        {
            _errorLine = deck.ParseDeckCSV(DeckInputText);
        }
        else
        {
            _errorLine = deck.ParseDeckCSV(deckInputTextList[0].placeholder.GetComponent<TMP_Text>().text);
        }

        // if there is a problem with the deck input, exit
        if (_errorLine > 0)
        {
            return false;
        }

        // output how many card types and cards successfully parsed
        LogManager.WriteNewLine("Deck has " + deck.CardTypes + " types of cards...");
        LogManager.WriteNewLine("Deck has " + deck.RemainingCards + " cards...");

        // initialize simulation
        RestartSimulation();
        IsRunningSimulation = true;

        //// make buttons that are used in the simulation interactable or not
        //EnableSimSelectables(true);

        // send event
        if (OnSimStart != null)
            OnSimStart();

        // return
        return true;
    }

    void StopSimulation()
    {
        // write stop simulation text
        LogManager.WriteNewLine("Simulation ended.\n");

        // reset flag
        IsRunningSimulation = false;

        // reset text
        RemainingText = "0";
        DiscardText = "0";

        // reset deck
        deck.ResetDeck();

        //// make buttons that are used in the simulation interactable or not
        //EnableSimSelectables(false);

        // send event
        if (OnSimEnd != null)
            OnSimEnd();
    }

    void RestartSimulation()
    {
        // initialize
        _draw = 1;

        // shuffle deck
        LogManager.WriteNewLine("\nShuffling Deck...");
        deck.ResetDeckThenShuffle();
        LogManager.WriteNewLine("Ready.\n");

        // reset labels
        ResetDeckLabels();
        ResetDrawnCard();
    }

    //void EnableSimSelectables(bool _enable)
    //{
    //    // make buttons that are used in the simulation interactable or not
    //    foreach (Selectable _selectable in simSelectables)
    //    {
    //        _selectable.interactable = _enable;
    //    }
    //}

    // ----------------------------------- UI Subfunctions
    void ResetDeckLabels()
    {
        DiscardText = "0";
        RemainingText = deck.RemainingCards.ToString();
    }

    void ResetDrawnCard()
    {
        DrawnCardName = "Card Name";
        DrawnCardText = "Card Text";
    }
}
