﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class RmRndPanelMgr : MonoBehaviour
{
    // ----------------------------------- Variables
    public TMP_Text qtyText, fullQtyText, cardName;
    public CardType associatedCardType;
    public Toggle removableToggle;
    public bool isRemovable = false;
    Color _zeroColor = new Color(0.2941177f, 0.2941177f, 0.2941177f, 1f);

    // ----------------------------------- Initialization
    void OnEnable()
    {
        // add on card drawn event
        Deck.Instance.OnCardDrawn += Instance_OnDeckCompoChanged;
        Deck.Instance.OnDeckCompoChanged += Instance_OnDeckCompoChanged;
    }

    public void OnDisable()
    {
        // remove on card drawn event
        Deck.Instance.OnCardDrawn -= Instance_OnDeckCompoChanged;
        Deck.Instance.OnDeckCompoChanged -= Instance_OnDeckCompoChanged;
    }

    public void InitializeRmRndPanel(CardType _cardTypeIn)
    {
        // save card type
        associatedCardType = _cardTypeIn;

        // get full qty
        fullQtyText.text = associatedCardType.deckCount.ToString();

        // set card name
        cardName.text = associatedCardType.cardArchetype.name;

        // set start qty and probability
        UpdateOutput();

        // reset color of qty text
        qtyText.color = Color.black;
    }

    // ----------------------------------- Button Functions
    public void OnRemovableToggled()
    {
        isRemovable = removableToggle.isOn;
    }

    // ----------------------------------- UI Subfunctions
    /// <summary>
    /// Updates the quantity values on the panels.
    /// </summary>
    public void UpdateOutput()
    {
        // update qty text
        qtyText.text = associatedCardType.remainingDeckCount.ToString();

        // get full qty
        fullQtyText.text = associatedCardType.deckCount.ToString();

        // change color if 0
        if (associatedCardType.remainingDeckCount == 0)
        {
            qtyText.color = _zeroColor;
        }
    }

    // ----------------------------------- Event Handlers
    /// <summary>
    /// updates qty stats when a card is drawn
    /// </summary>
    /// <param name="_cardArg"></param>
    private void Instance_OnDeckCompoChanged(Card _cardArg)
    {
        // update panel
        UpdateOutput();
    }
}
