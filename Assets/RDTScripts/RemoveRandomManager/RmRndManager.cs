﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class RmRndManager : MonoBehaviour
{
    // ----------------------------------- Variables
    public List<RmRndPanelMgr> rmRndPanels;
    public Transform rmRndContentGO;
    public GameObject rmRndPanelPrefab;
    public Deck deck;
    public Toggle toggleAll;

    // ----------------------------------- Properties
    public static RmRndManager Instance { get; private set; }

    // ----------------------------------- Initialization and Destruction
    void Awake()
    {
        Instance = this;
    }

    void OnEnable()
    {
        // initialize list
        rmRndPanels = new List<RmRndPanelMgr>();

        // add DeckUIManager events
        DeckUIManager.OnSimStart += OnSimStartHandler;
        DeckUIManager.OnSimEnd += OnSimEndHandler;

        // add deck events
        Deck.Instance.OnResetRemainingCardCounts += OnResetRemainingCardCountsHandler;
    }

    private void OnDisable()
    {
        // remove DeckUIManager
        DeckUIManager.OnSimStart -= OnSimStartHandler;
        DeckUIManager.OnSimEnd -= OnSimEndHandler;

        // remove deck events
        Deck.Instance.OnResetRemainingCardCounts -= OnResetRemainingCardCountsHandler;
    }

    // ----------------------------------- Event Handling
    /// <summary>
    /// Initializes the remove random manager when the simulation start event is called by DeckUIManager
    /// </summary>
    private void OnSimStartHandler()
    {
        // get number of card types then populate remove random panel list
        for (int _ctr = 0; _ctr < deck.CardTypes; _ctr++)
        {
            // instantaite panel prefab
            RmRndPanelMgr _temp = Instantiate(rmRndPanelPrefab, rmRndContentGO, false).GetComponent<RmRndPanelMgr>();

            // add panel prefab to prob panels list
            rmRndPanels.Add(_temp);

            // initialize panel
            _temp.InitializeRmRndPanel(deck.cardTypesList[_ctr]);
        }
    }

    /// <summary>
    /// Cleanup when a sim is ended. Destroy prob panels and new list
    /// </summary>
    private void OnSimEndHandler()
    {
        // destroy probabilty panels
        foreach (RmRndPanelMgr _panel in rmRndPanels)
        {
            // destroy each active panel (i keep a prefab inactive for easy editing's take)
            if (_panel.gameObject.activeSelf)
            {
                Destroy(_panel.gameObject);
            }
        }

        // initialize list
        rmRndPanels = new List<RmRndPanelMgr>();
    }

    private void OnResetRemainingCardCountsHandler(Card _cardArg)
    {
        UpdateAll();
    }

    // ----------------------------------- UI Functions
    /// <summary>
    /// For when necessary, updates all output UI to state of the deck
    /// </summary>
    public void UpdateAll()
    {
        // update each panel
        foreach (RmRndPanelMgr _panel in rmRndPanels)
        {
            _panel.UpdateOutput();
        }
    }

    // ----------------------------------- Button Handling -----------------------------------
    public void OnRemoveReshuffleButtonPressed()
    {
        // get list of cards we can remove
        LogManager.WriteNewLine("Removing 1 Random card...");
        LogManager.WriteNewLine("Listing card types that can be removed...");
        List<CardType> _removableCardTypes = new List<CardType>();
        foreach (RmRndPanelMgr _panelMgr in rmRndPanels)
        {
            // to be a valid removable, has to be marked as removable and there are still cards of that type in the deck
            if (_panelMgr.isRemovable && _panelMgr.associatedCardType.remainingDeckCount > 0)
                _removableCardTypes.Add(_panelMgr.associatedCardType);
        }

        // if list is empty, it means no more cards can be removed so return
        if (_removableCardTypes.Count == 0)
        {
            LogManager.WriteNewLine("No valid cards flagged to be removed. Either none are flagged or card types are 0 in the deck. Aborting Random Removal.");
            return;
        }

        LogManager.WriteNewLine("Done. 1 Random card will be removed from one of " + _removableCardTypes.Count.ToString() + " flagged card types.");

        // choose a random the card type to be removed
        LogManager.Write("Choosing random card type to be removed...");
        int _randIndex = Random.Range(0, _removableCardTypes.Count);
        Card _cardToBeRemoved = _removableCardTypes[_randIndex].cardArchetype;
        LogManager.WriteNewLine("Done.");

        // get index of the card to be removed in the actual deck
        LogManager.Write("Removing card...");
        int _cardIndex = Deck.Instance.FindCardType(_cardToBeRemoved);

        // remove card and update deck type info
        Deck.Instance.RemoveCardAtIndex(_cardIndex, true);
        LogManager.WriteNewLine("Done.");

        // shuffle deck
        LogManager.Write("Shuffling Deck...");
        Deck.Instance.ShuffleCards();
        LogManager.WriteNewLine("Done.");
    }

    public void OnToggleAllPressed()
    {
        // get value of toggle all
        bool _temp = toggleAll.isOn;

        // change value of each panel
        foreach (RmRndPanelMgr _rmPanel in rmRndPanels)
        {
            _rmPanel.isRemovable = _temp;
            _rmPanel.removableToggle.isOn = _temp;
        }
    }
}
